#!/bin/bash

CMDS="$1"
if [ -z "$CMDS" ]; then
    CMDS="run"
fi
rm -rf $PWD/target
mkdir -p $PWD/deploy/scm-manager
rm -rf $PWD/deploy/plugins/com/morkid
mvn scmp:$CMDS -DscmHome=$PWD/deploy -Dbasedir=$PWD/deploy/scm-manager
