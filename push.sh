#!/bin/bash

ACTIVE_BRANCH=$(git rev-parse --abbrev-ref HEAD)
COMMENT="$1"

if [ -z "$COMMENT" ]; then
    COMMENT="autocommit"
fi

read -p "Push to branch '$ACTIVE_BRANCH'? (YES/NO). [YES]: " ANSWER

case $ANSWER in
    "y"|"Y"|"yes"|"YES"|"Yes"|"")
        ANSWER="YES"
    ;;
esac

if [ "$ANSWER" = "YES" ]; then
    git add -A .
    git commit -m "$COMMENT"
    git push -u origin $ACTIVE_BRANCH
else
    echo "Push aborted."
fi
