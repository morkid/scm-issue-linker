#!/bin/bash

CURRENT_DIRECTORY="$PWD"

if [ ! -d "$CURRENT_DIRECTORY/src/main/java" ]; then
    read -p "Input artifactId (allowed characters[a-z0-9\\-]): " PACKAGE_ARTIFACT_ID
    if [ -z "$PACKAGE_ARTIFACT_ID" ]; then
        echo "Artifact id could not be empty."
        exit 0
    fi

    read -p "Input groupId (allowed characters[a-zA-Z\\.]): " PACKAGE_GROUP_ID
    if [ -z "$PACKAGE_GROUP_ID" ]; then
        echo "Group id could not be empty."
        exit 0
    fi

    read -p "Input version (allowed characters[a-z0-9\\-\\.]): " PACKAGE_VERSION
    if [ -z "$PACKAGE_VERSION" ]; then
        PACKAGE_VERSION="1.0"
    fi
    
    mvn archetype:generate\
       -DartifactId="$PACKAGE_ARTIFACT_ID"\
       -DgroupId="$PACKAGE_GROUP_ID"\
       -Dversion="$PACKAGE_VERSION"\
       -DarchetypeGroupId=sonia.scm.maven\
       -DarchetypeArtifactId=scm-plugin-archetype\
       -DarchetypeVersion=1.50\
       -DarchetypeRepository=http://maven.scm-manager.org/nexus/content/groups/public/

    echo "please wait..."
    sleep 2
    echo "change directory to $PWD/$PACKAGE_ARTIFACT_ID"
    if [ -d "$PWD/$PACKAGE_ARTIFACT_ID" ]; then
        cd "$PWD/$PACKAGE_ARTIFACT_ID"
        CURRENT_DIRECTORY="$PWD"
    fi
fi
SOURCE_DIRECTORY="$CURRENT_DIRECTORY/src/main/java"

if [ ! -d "$SOURCE_DIRECTORY" ]; then
    exit 0
fi
PACKAGE_URL="https://bitbucket.org/morkid/scm-issue-linker"
YEAR="$(date +%Y)"
AUTHOR="Morkid <morkid@gmail.com>"
MY_JAVA_PACKAGES="$(cd $SOURCE_DIRECTORY && find . -type f -name '*.java')"
JAVA_PACKAGE_LIST=$(echo -e "$MY_JAVA_PACKAGES")

for o in $JAVA_PACKAGE_LIST ; do
    STR=$(echo "$o" | sed "s|^./||g" | sed "s|/|.|g" | sed "s|.java||g")
    a="${STR%.*}"
    PACKAGE_NAME="${STR%.*}"
    CLASS_NAME="${STR##*.}"
    CLASS_TEMPLATE=$(cat <<JAVACLASSTEMPALTE
package ${PACKAGE_NAME};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ${CLASS_NAME}
 *
 * @author Morkid <morkid@gmail.com>
 */
public class ${CLASS_NAME}
{
    private static final Logger logger = LoggerFactory.getLogger(${CLASS_NAME}.class);

    public ${CLASS_NAME}()
    {

    }

}
JAVACLASSTEMPALTE
)

    echo "$CLASS_TEMPLATE" > $SOURCE_DIRECTORY/$o
done