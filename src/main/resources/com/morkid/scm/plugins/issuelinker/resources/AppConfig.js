Ext.ns("Morkid.issuelinker");
if (window.debug) {
    window.debug = false;
}
if (window.console) {
    window.console.warn = function() {};
    window.console.debug = function() {};
    // window.console.log = function(){};
}
Ext.apply(Ext.form.VTypes, {
    linkerUrl: function(val) {
        return val.match(this.linkerUrlTextRegex);
    },
    linkerUrlText: 'Please insert a valid IssueLinker url',
    linkerUrlTextRegex: /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/
});

Morkid.issuelinker.AppConfigPanel = Ext.extend(Sonia.config.ConfigForm, {

    // errors
    errorBoxTitle: 'Error',
    errorOnSubmitText: 'Error during config submit.',
    errorOnLoadText: 'Error during config load.',

    initComponent: function() {
        var config = {
            title: 'IssueLinker Configuration',
            items: [{
                xtype: 'textfield',
                fieldLabel: "Base Url",
                name: 'url',
                vtype: 'linkerUrl',
                allowBlank: true,
                helpText: 'Base Url of IssueLinker installation (with contextpath).'
            }, {
                id: 'linkerKeys',
                xtype: 'textfield',
                name: 'keys',
                fieldLabel: "Issue Keywords",
                property: 'keys',
                helpText: 'Issue Keywords'
            }, {
                xtype: 'textfield',
                id: 'linkerPathLink',
                name: 'path-link',
                fieldLabel: "Path Link",
                property: 'path-link',
                helpText: 'Path link'
            }, {
                xtype: 'textfield',
                id: 'linkerPathList',
                name: 'path-list',
                fieldLabel: "Path List",
                property: 'path-list',
                helpText: 'Path List'
            }, {
                xtype: 'textfield',
                id: 'linkerPathUpdate',
                name: 'path-update',
                fieldLabel: "Update Path",
                property: 'path-update',
                helpText: 'Update Path'
            }, {
                xtype: 'textfield',
                id: 'linkerUsername',
                name: 'username',
                fieldLabel: 'Username',
                property: 'username',
                helpText: 'IssueLinker username for connection. Leave this field empty to create the connection\n\
                     with the credentials of the user which is logged in.'
            }, {
                xtype: 'textfield',
                id: 'linkerPassword',
                name: 'password',
                fieldLabel: 'Password',
                property: 'password',
                inputType: 'password',
                helpText: 'IssueLinker password for connection.'
            }, {
                xtype: 'checkbox',
                fieldLabel: 'Disable repository configuration',
                name: 'disable-repository-configuration',
                inputValue: 'true',
                helpText: 'Do not allow repository owners to configure IssueLinker instances. ' +
                    'You have to restart your application server after changing this value.'
            }]
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        Morkid.issuelinker.AppConfigPanel.superclass.initComponent.apply(this, arguments);
    },

    onSubmit: function(values) {
        this.el.mask(this.submitText);
        Ext.Ajax.request({
            url: restUrl + 'plugins/morkid/issuelinker/issues/app-config.json',
            method: 'POST',
            jsonData: values,
            scope: this,
            disableCaching: true,
            success: function(response) {
                this.el.unmask();
            },
            failure: function() {
                this.el.unmask();
                Ext.MessageBox.show({
                    title: this.errorBoxTitle,
                    msg: this.errorOnSubmitText,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        });
    },

    onLoad: function(el) {
        var tid = setTimeout(function() {
            el.mask(this.loadingText);
        }, 100);
        Ext.Ajax.request({
            url: restUrl + 'plugins/morkid/issuelinker/issues/app-config.json',
            method: 'GET',
            scope: this,
            disableCaching: true,
            success: function(response) {
                var obj = Ext.decode(response.responseText);
                this.load(obj);

                clearTimeout(tid);
                el.unmask();
            },
            failure: function() {
                el.unmask();
                clearTimeout(tid);
                Ext.MessageBox.show({
                    title: this.errorBoxTitle,
                    msg: this.errorOnLoadText,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        });
    }

});

// register xtype
Ext.reg("issueLinkerAppConfigPanel", Morkid.issuelinker.AppConfigPanel);

// regist config panel
registerGeneralConfigPanel({
    id: 'issueLinkerAppConfigPanel',
    xtype: 'issueLinkerAppConfigPanel'
});

Sonia.repository.ExtendedInfoPanel.prototype.issueModifyDefaultConfig = Sonia.repository.ExtendedInfoPanel.prototype.modifyDefaultConfig;
Ext.override(Sonia.repository.ExtendedInfoPanel, {
    modifyDefaultConfig: function(config) {
        this.issueModifyDefaultConfig(config);
        var box = config.items[config.items.length - 1].items;
        box.push({
            xtype: "box",
            html: ", ",
            width: 8
        }, {
            xtype: "link",
            style: "font-weight: bold",
            text: "List Issues",
            handler: this.addIssueLinker,
            scope: this
        })
    },
    addIssueLinker: function() {
        main.addTab({
            id: "issueLinkerGrid;" + this.item.id + ";null",
            xtype: "issueLinkerGrid",
            repository: this.item,
            closable: true
        })
    }
});
