Ext.ns("Morkid.issuelinker");
Morkid.issuelinker.IssueLinkerGrid = Ext.extend(Sonia.rest.Grid, {
    repository: null,
    forceFit: true,
    loadingText: "Loading ...",
    mainGroup: 'main',
    filterRequest: null,

    /**
     * @deprecated use filterRequest
     */
    searchValue: null,

    /**
     * @deprecated use filterRequest
     */
    typeFilter: null,
    linkTemplate: '\x3ca class\x3d"scm-link issue-linker-pr-id" target\x3d"_blank" href\x3d"{0}"\x3e#{1}\x3c/a\x3e',
    initComponent: function() {

        var issueStore = new Ext.data.GroupingStore({
            proxy: new Ext.data.HttpProxy({
                url: restUrl + "plugins/morkid/issuelinker/issues/lists/" + this.repository.id + ".json",
                method: "GET",
                disableCaching: false
            }),
            idProperty: 'id',
            reader: new Ext.data.JsonReader({
                fields: [{
                    name: 'id'
                }, {
                    name: 'group',
                    convert: this.convertToGroup
                }, {
                    name: 'title'
                }, {
                    name: 'status',
                    sortType: 'asUCString'
                }, {
                    name: 'level'
                }, {
                    name: 'message'
                }, {
                    name: 'url'
                }]
            }),
            sortInfo: {
                field: 'status'
            },
            autoDestroy: true,
            autoLoad: true,
            remoteGroup: false,
            groupOnSort: false,
            remoteSort: false,
            groupField: 'group',
            groupDir: 'AES',
            listeners: {
                exception: Sonia.rest.ExceptionHandler
            }
        });
        var issueModel = new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                scope: this,
                width: 125
            },
            columns: [{
                id: 'id',
                dataIndex: 'id',
                header: "Id",
                renderer: this.renderId,
                scope: this
            }, {
                id: 'status',
                header: 'Status',
                dataIndex: 'status',
                scope: this,
                listeners: {
                    click: {
                        fn: this.onClickRow,
                        scope: this
                    }
                }
            }, {
                id: 'level',
                header: 'Level',
                dataIndex: 'level',
                scope: this,
                listeners: {
                    click: {
                        fn: this.onClickRow,
                        scope: this
                    }
                }
            }, {
                id: 'url',
                header: 'Url',
                dataIndex: 'url',
                hidden: true,
                hideable: false,
                scope: this
            }, {
                id: 'title',
                header: 'Title',
                dataIndex: 'title',
                scope: this,
                listeners: {
                    click: {
                        fn: this.onClickRow,
                        scope: this
                    }
                }
            }, {
                id: 'message',
                header: 'Message',
                dataIndex: 'message',
                hidden: true,
                hideable: false,
                scope: this
            }, {
                id: 'group',
                dataIndex: 'group',
                hidden: true,
                hideable: false,
                groupRenderer: this.renderGroupName,
                scope: this
            }]
        });

        var toolbar = new Ext.Toolbar();
        var btn = new Ext.Toolbar.Button({
            text: 'Refresh',
            icon: 'resources/images/reload.png',
            handler: function() {
                this.getStore().reload();
            },
            scope: this
        });
        toolbar.add(new Ext.Template('<div style="margin-left: 5px;">Click an issue to view detail</div>'));
        toolbar.add({ xtype: 'tbseparator' });
        toolbar.add(btn);

        var config = {
            title: this.repository.name + " Issues",
            autoExpandColumn: 'title',
            store: issueStore,
            colModel: issueModel,
            tbar: toolbar,
            emptyText: "No issues available.",
            view: new Ext.grid.GroupingView({
                idPrefix: '{grid.id}',
                enableGrouping: true,
                enableNoGroups: false,
                forceFit: true,
                groupMode: 'value',
                enableGroupingMenu: true,
                groupTextTpl: '{group} ({[values.rs.length]} {[values.rs.length > 1 ? "Issues" : "Issue"]})',
                getRowClass: function(record) {}
            })
        };
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        Morkid.issuelinker.IssueLinkerGrid.superclass.initComponent.apply(this, arguments);

        if (this.parentPanel) {
            this.parentPanel.issueGrid = this;
        }
    },
    convertToGroup: function(v, data) {
        return [data.status, data.level].join(' ').toUpperCase();
    },
    renderGroupName: function(v, data) {
        return v;
    },
    renderId: function(id, a, row, index) {
        return String.format(this.linkTemplate, row.data.url, id);
    },
    onClickRow: function(me, el, even) {
        var sel = this.getSelectionModel();
        var selected = sel.getSelected() || {};
        if (!selected.data) {
            return;
        }
        var data = selected.data;
        Ext.Msg.show({
            id: 'grid-preview',
            title: "Issue: #" + data.id + ", Level: " + data.level + ", Status: " + data.status + ", " + data.title,
            msg: '<div style="display:block;width:100%;height:400px;max-height:600px;overflow:auto;background-color: #fff;">' +
                data.message + '</div>',
            buttons: Ext.MessageBox.OK,
            fn: function() {
                sel.clearSelections();
            },
            minWidth: window.innerWidth - 250,
            maxWidth: window.innerWidth,
            buttonText: {
                ok: "Close"
            },
            animEl: 'elId'
        });

    }
});
Ext.reg("issueLinkerGrid", Morkid.issuelinker.IssueLinkerGrid);
