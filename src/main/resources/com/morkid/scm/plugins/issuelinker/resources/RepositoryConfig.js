Ext.ns("Morkid.issuelinker");
Morkid.issuelinker.RepositoryConfigPanel = Ext.extend(Sonia.repository.PropertiesFormPanel, {

    initComponent: function() {
        var config = {
            title: 'IssueLinker Configuration',
            items: [{
                xtype: 'textfield',
                fieldLabel: "Base Url",
                name: 'url',
                vtype: 'linkerUrl',
                property: 'issuelinker.url',
                allowBlank: true,
                helpText: 'Base Url of IssueLinker installation (with contextpath).'
            }, {
                id: 'linkerRepoKeys',
                name: 'keys',
                fieldLabel: "Issue Keywords",
                property: 'issuelinker.keys',
                helpText: 'Issue Keywords'
            }, {
                id: 'linkerRepoPathLink',
                name: 'pathLink',
                fieldLabel: "Path Link",
                property: 'issuelinker.path-link',
                helpText: 'Path link'
            }, {
                id: 'linkerRepoPathList',
                name: 'pathList',
                fieldLabel: "Path List",
                property: 'issuelinker.path-list',
                helpText: 'Path List'
            }, {
                id: 'linkerRepoPathUpdate',
                name: 'pathUpdate',
                fieldLabel: "Update Path",
                property: 'issuelinker.path-update',
                helpText: 'Update Path'
            }, {
                id: 'linkerRepoUsername',
                name: 'username',
                fieldLabel: 'Username',
                property: 'issuelinker.username',
                helpText: 'IssueLinker username for connection. Leave this field empty to create the connection\n\
                     with the credentials of the user which is logged in.'
            }, {
                id: 'linkerRepoPassword',
                name: 'password',
                fieldLabel: 'Password',
                property: 'issuelinker.password',
                inputType: 'password',
                helpText: 'IssueLinker password for connection.'
            }]
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        Morkid.issuelinker.RepositoryConfigPanel.superclass.initComponent.apply(this, arguments);
    }

});

// register xtype
Ext.reg("morkidRepositoryConfigPanel", Morkid.issuelinker.RepositoryConfigPanel);

// register panel
Sonia.repository.openListeners.push(function(repository, panels) {
    if (Sonia.repository.isOwner(repository)) {
        panels.push({
            xtype: 'morkidRepositoryConfigPanel',
            item: repository
        });
    }
});
