package com.morkid.scm.plugins.issuelinker;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.morkid.scm.plugins.issuelinker.config.AppConfig;
import com.morkid.scm.plugins.issuelinker.config.RepositoryConfig;
import com.morkid.scm.plugins.issuelinker.tracker.IssueMatcherTracker;
import com.morkid.scm.plugins.issuelinker.tracker.IssueTracker;
import com.morkid.scm.plugins.issuelinker.tracker.RepositoryTracker;
import com.morkid.scm.plugins.issuelinker.handler.ChangesetHandler;
import com.morkid.scm.plugins.issuelinker.util.HttpRequestUtil;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.config.ScmConfiguration;
import sonia.scm.issuetracker.*;
import sonia.scm.plugin.ext.Extension;
import sonia.scm.repository.Repository;
import sonia.scm.security.Role;
import sonia.scm.store.DataStoreFactory;
import sonia.scm.issuetracker.ChangeStateHandler;
import sonia.scm.store.Store;
import sonia.scm.store.StoreFactory;
import sonia.scm.template.TemplateEngineFactory;

import javax.json.JsonObject;
import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * IssueLinker
 * @author Morkid <morkid@gmail.com>
 */
@Singleton
@Extension
public class IssueLinker extends DataStoreBasedIssueTrack {
    private static final String NAME = "issuelinker";

    private final Provider<LinkHandler> linkHandlerProvider;

    private final Store<AppConfig> store;

    private final TemplateEngineFactory templateEngineFactory;

    private static final Logger logger = LoggerFactory.getLogger(IssueLinker.class);

    private ScmConfiguration configuration;

    @Inject
    public IssueLinker(StoreFactory storeFactory,
                       DataStoreFactory dataStoreFactory,
                       TemplateEngineFactory templateEngineFactory,
                       Provider<LinkHandler> linkHandlerProvider,
                       ScmConfiguration configuration) {
        super(NAME, dataStoreFactory);
        this.store = storeFactory.getStore(AppConfig.class, NAME);
        this.templateEngineFactory = templateEngineFactory;
        this.linkHandlerProvider = linkHandlerProvider;
        this.configuration = configuration;
    }

    @Override
    public IssueMatcherTracker createMatcher(Repository repository) {
        IssueMatcherTracker matcher = null;
        RepositoryConfig config = configResolver(repository);

        if (config != null) {
            matcher = new IssueMatcherTracker(config);
        }

        return matcher;
    }

    @Override
    protected ChangeStateHandler getChangeStateHandler(IssueRequest request) {
        ChangeStateHandler changeStateHandler = null;
        RepositoryConfig conf = configResolver(request.getRepository());

        if ((conf != null) && conf.isValid()) {
            changeStateHandler = new ChangesetHandler(templateEngineFactory,
                    linkHandlerProvider.get(), conf, request, configuration);
        } else {
            logger.debug("configuration is not valid or change state is disabled");
        }

        return changeStateHandler;
    }

    @Override
    protected CommentHandler getCommentHandler(IssueRequest request) {
        CommentHandler commentHandler = null;
        RepositoryConfig conf = configResolver(request.getRepository());

        if ((conf != null) && conf.isValid()) {
            commentHandler = new com.morkid.scm.plugins.issuelinker.handler.CommentHandler(
                templateEngineFactory, linkHandlerProvider.get(), conf, request, configuration);
        } else {
            logger.debug("configuration is not valid or update is disabled");
        }

        return commentHandler;
    }

    public RepositoryConfig getDefaultConfig() {
        AppConfig config = store.get();

        if (config == null) {
            config = new AppConfig();
        }

        return config;
    }

    public void setDefaultConfig(AppConfig conf) {
        Subject subject = SecurityUtils.getSubject();

        subject.checkRole(Role.ADMIN);
        store.set(conf);
    }

    public List<IssueTracker> getIssues(RepositoryTracker repo) {
        List<IssueTracker> issues = new ArrayList<IssueTracker>();

        if (null != repo) {
            RepositoryConfig config = configResolver(repo.get());

            if (null != config && config.isValid()) {
                MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
                Subject subject = SecurityUtils.getSubject();
                String username = "";
                if (subject.hasRole(Role.USER)) {
                    username = (String) subject.getPrincipal();
                }

                formData.add("id", repo.getId());
                formData.add("name", repo.getName());
                formData.add("type", repo.getType());
                formData.add("contact", repo.getContact());
                formData.add("url", repo.getUrl());
                formData.add("username", username);

                HttpRequestUtil http = new HttpRequestUtil(config.getFullPathList());
                http.setUserPass(config.getUsername(), config.getPassword());
                http.get(formData);
                ListIterator it = http.getResult();

                while ( it.hasNext() ) {
                    IssueTracker si = new IssueTracker();
                    JsonObject value = (JsonObject) it.next();

                    si.setId(value.getString("id"));
                    si.setTitle(value.getString("title"));
                    si.setStatus(value.getString("status"));
                    si.setMessage(value.getString("message"));
                    si.setLevel(value.getString("level"));
                    si.setUrl(value.getString("url"));

                    issues.add(si);
                }
            }
        }

        return issues;
    }

    private RepositoryConfig configResolver(Repository repository) {
        RepositoryConfig defaultConfig = getDefaultConfig();
        RepositoryConfig conf = new RepositoryConfig();
        if (!defaultConfig.isRepositoryConfigDisabled()) {
            conf = new AppConfig(repository);
        }
        if (!conf.isValid()) {
            logger.debug("repository config for {} is not valid try global one",
                         repository.getName());
            conf = defaultConfig;
        }

        if (!conf.isValid()) {
            logger.debug("no valid configuration for repository {} found",
                         repository.getName());
            conf = null;
        }
        return conf;
    }

}
