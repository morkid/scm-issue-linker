package com.morkid.scm.plugins.issuelinker.resource;

import com.google.inject.Inject;
import com.morkid.scm.plugins.issuelinker.IssueLinker;
import com.morkid.scm.plugins.issuelinker.tracker.RepositoryTracker;
import org.apache.shiro.SecurityUtils;
import sonia.scm.security.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.WebApplicationException;

/**
 * IssueResource
 *
 * @author Morkid <morkid@gmail.com>
 */
@Path("plugins/morkid/issuelinker/issues")
public class IssueResource {
    private static final Logger logger = LoggerFactory.getLogger(IssueResource.class);

    private String username;

    @Inject
    public IssueResource(IssueLinker linker, RepositoryTracker factory) {
        this.linker = linker;
        this.factory = factory;
    }

    @GET
    @Path("lists/{repoId}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getIssues(@PathParam("repoId") String repoId) {
        if (!SecurityUtils.getSubject().hasRole(Role.USER)) {
            logger.warn("user has not enough privileges to view issues");

            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }
        RepositoryTracker f = factory.reset().find(repoId);

        return Response.ok(linker.getIssues(f)).build();
    }

    private IssueLinker linker;

    private RepositoryTracker factory;

}
