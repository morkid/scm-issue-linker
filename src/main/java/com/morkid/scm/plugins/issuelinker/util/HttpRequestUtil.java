package com.morkid.scm.plugins.issuelinker.util;

import com.google.common.base.Strings;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.ListIterator;

public class HttpRequestUtil
{
    private static final Logger logger = LoggerFactory.getLogger(HttpRequestUtil.class);

    private ListIterator iterator;

    private String url;

    private String username;

    private String password;

    public HttpRequestUtil(String url)
    {
        this.url = url;
        this.iterator = (new ArrayList()).listIterator();
    }

    public HttpRequestUtil setUserPass(String username, String password)
    {
        this.username = username;
        this.password = password;

        return this;
    }

    public void get(MultivaluedMap<String, String> data)
    {
        try {
            Client client = Client.create();
            if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
                client.addFilter(new HTTPBasicAuthFilter(username, password));
            }
            WebResource webResource = client.resource(url);
            ClientResponse response = webResource
                    .queryParams(data)
                    .type(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_FORM_URLENCODED)
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            String jsonData = response.getEntity(String.class);
            JsonReader jsonReader = Json.createReader(new StringReader(jsonData));
            JsonArray arr = jsonReader.readArray();
            jsonReader.close();
            iterator = arr.listIterator();
        }
        catch (ClientHandlerException e) {
            // .. no catch
        }
        catch(RuntimeException e) {
            // .. no catch
        }
    }

    public void post(MultivaluedMap<String, String> data)
    {
        try {
            Client client = Client.create();
            if (!Strings.isNullOrEmpty(username) && !Strings.isNullOrEmpty(password)) {
                client.addFilter(new HTTPBasicAuthFilter(username, password));
            }

            WebResource webResource = client.resource(url);
            ClientResponse response = webResource
                    .type(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                    .post(ClientResponse.class, data);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }
        }
        catch (ClientHandlerException e) {
            // .. no catch
        }
        catch(RuntimeException e) {
            // .. no catch
        }
    }

    public HttpRequestUtil setUrl(String url) {
        this.url = url;

        return this;
    }

    public HttpRequestUtil reset(String url)
    {
        this.url = url;
        this.iterator = (new ArrayList()).listIterator();

        return this;
    }

    public ListIterator getResult()
    {
        return iterator;
    }

}
