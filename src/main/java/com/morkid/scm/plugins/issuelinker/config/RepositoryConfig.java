package com.morkid.scm.plugins.issuelinker.config;

import com.google.common.base.Strings;
import com.morkid.scm.plugins.issuelinker.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.PropertiesAware;
import sonia.scm.Validateable;
import sonia.scm.issuetracker.PropertyUtil;
import sonia.scm.issuetracker.XmlEncryptionAdapter;
import sonia.scm.repository.Repository;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;

/**
 * RepositoryConfig
 *
 * @author Morkid <morkid@gmail.com>
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RepositoryConfig implements Validateable
{
    protected static final String PROP_URL = "issuelinker.url";

    protected static final String PROP_PATH_LIST = "issuelinker.path-list";

    protected static final String PROP_PATH_UPDATE = "issuelinker.path-update";

    protected static final String PROP_PATH_LINK = "issuelinker.path-link";

    protected static final String PROP_KEYS = "issuelinker.keys";

    protected static final String PROP_USERNAME = "issuelinker.username";

    protected static final String PROP_PASSWORD = "issuelinker.password";

    protected static final String SEPARATOR = ",";

    @XmlJavaTypeAdapter(XmlEncryptionAdapter.class)
    private String password;

    @XmlElement(name = "path-list")
    private String pathList;

    @XmlElement(name = "path-update")
    private String pathUpdate;

    @XmlElement(name = "path-link")
    private String pathLink;

    private String username;

    private String url;

    private String keys;

    private static final Logger logger = LoggerFactory.getLogger(RepositoryConfig.class);

    public RepositoryConfig(){}

    public RepositoryConfig(Repository repo) {
        url = repo.getProperty(PROP_URL);
        username = repo.getProperty(PROP_USERNAME);
        password = PropertyUtil.getEncryptedProperty(repo, PROP_PASSWORD);
        pathList = repo.getProperty(PROP_PATH_LIST);
        pathLink = repo.getProperty(PROP_PATH_LINK);
        pathUpdate = repo.getProperty(PROP_PATH_UPDATE);
        keys = repo.getProperty(PROP_KEYS);
    }

    public RepositoryConfig(PropertiesAware props) {
        url = props.getProperty(PROP_URL);
        username = props.getProperty(PROP_USERNAME);
        password = PropertyUtil.getEncryptedProperty(props, PROP_PASSWORD);
        pathList = props.getProperty(PROP_PATH_LIST);
        pathLink = props.getProperty(PROP_PATH_LINK);
        pathUpdate = props.getProperty(PROP_PATH_UPDATE);
        keys = props.getProperty(PROP_KEYS);
    }

    @Override
    public boolean isValid() {
        return !Strings.isNullOrEmpty(url);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPathList() {
        return pathList;
    }

    public void setPathList(String pathList) {
        this.pathList = pathList;
    }

    public String getPathUpdate() {
        return pathUpdate;
    }

    public void setPathUpdate(String pathUpdate) {
        this.pathUpdate = pathUpdate;
    }

    public String getPathLink() {
        return pathLink;
    }

    public void setPathLink(String pathLink) {
        this.pathLink = pathLink;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }

    public String getFullPathUpdate() {
        return fixUrlPath(pathUpdate);
    }

    public String getFullPathList() {
        return fixUrlPath(pathList);
    }

    public String getFullPathLink() {
        return fixUrlPath(pathLink);
    }

    public Iterable<String> getKeywords()
    {
        if (Strings.isNullOrEmpty(keys)) {
            return new ArrayList<String>();
        }
        Iterable<String> iterable = StringUtil.explodeTrimSpaceToList(keys, SEPARATOR);

        return  iterable;
    }

    private String fixUrlPath(String path) {
        String uri = StringUtil.trim(url, "/");
        if (Strings.isNullOrEmpty(path)) {
            if (Strings.isNullOrEmpty(uri)) {
                return url;
            }
        }
        String newPath = StringUtil.trim(path, "/");
        String[] paths = {uri, newPath};

        return StringUtil.buildUnixPath(paths);
    }

    public boolean isRepositoryConfigDisabled() {
        return false;
    }
}
