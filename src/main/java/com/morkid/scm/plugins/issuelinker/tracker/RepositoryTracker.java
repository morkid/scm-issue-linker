package com.morkid.scm.plugins.issuelinker.tracker;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.config.ScmConfiguration;
import sonia.scm.repository.Repository;
import sonia.scm.repository.RepositoryNotFoundException;
import sonia.scm.repository.api.RepositoryService;
import sonia.scm.repository.api.RepositoryServiceFactory;
import sonia.scm.util.HttpUtil;

import javax.servlet.http.HttpServletRequest;

public class RepositoryTracker {
    private static final Logger logger = LoggerFactory.getLogger(RepositoryTracker.class);

    private RepositoryServiceFactory factory;

    private Repository repo;

    private boolean hasRepo = false;

    private ScmConfiguration configuration;

    private HttpServletRequest request;

    @Inject
    public RepositoryTracker(RepositoryServiceFactory factory, HttpServletRequest request) {
        this.factory = factory;
        this.request = request;
    }

    public RepositoryTracker(String repoId) {
        if (Strings.isNullOrEmpty(repoId)) {
            try {
                this.set(repoId);
            } catch (RepositoryNotFoundException e) {
                // ..
            }
        }
    }

    public void setRepo(Repository repo) {
        reset();
        if (null != repo) {
            this.repo = repo;
            this.hasRepo = true;
        }
    }

    private void set(String id) throws RepositoryNotFoundException {
        RepositoryService service = null;
        try {
            service = factory.create(id);
            this.repo = service.getRepository();
            this.hasRepo = true;
        } finally {
            if (service != null) {
                service.close();
            }
        }
    }

    public Repository get() {
        return this.repo;
    }

    public RepositoryTracker find(String id) {
        if (this.repo == null || !this.hasRepo) {
            try {
                this.set(id);
            } catch (RepositoryNotFoundException e) {
                // ..
            }
        }
        return this;
    }

    public RepositoryTracker reset() {
        this.repo = null;
        this.hasRepo = false;

        return this;
    }

    private String getItem(String key) {

        if (null == repo || !hasRepo) {
            return "";
        }

        String result;

        if (key.equals("name")) {
            result = repo.getName();
        } else if (key.equals("id")) {
            result = repo.getId();
        } else if (key.equals("contact")) {
            result = repo.getContact();
        } else if (key.equals("type")) {
            result = repo.getType();
        } else if (key.equals("url")) {
            result = repo.createUrl(HttpUtil.getCompleteUrl(request));
        } else {
            result = "";
        }

        return result;
    }

    public String getName() {
        return getItem("name");
    }

    public String getId() {
        return getItem("id");
    }

    public String getContact() {
        return getItem("contact");
    }

    public String getType() {
        return getItem("type");
    }

    public String getUrl() {
        return getItem("url");
    }
}
