/**
 * Copyright (c) 2017, Morkid <morkid@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of SCM-Manager; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * https://bitbucket.org/morkid/scm-issue-linker
 *
 */
package com.morkid.scm.plugins.issuelinker.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * StringUtil
 *
 * @author Morkid <morkid@gmail.com>
 */
public class StringUtil
{
    private static final Logger logger = LoggerFactory.getLogger(StringUtil.class);

    public StringUtil()
    {
    }

    public static String trim(String str, String pattern)
    {
        if (null == str) {
            return "";
        }
        return str.replaceAll("^[" + pattern + "]+|[" + pattern + "]+$", "");
    }

    public static String trim(String str)
    {
        return StringUtil.trim(str, "^A-z0-9");
    }

    public static String trimSlashes(String str)
    {
        return StringUtil.trim(str, "/");
    }

    public static String trimSpaces(String str)
    {
        return StringUtil.trim(str, "\\s");
    }

    public static String[] explode(String exploder, String str)
    {
        return str.split(exploder);
    }

    public static String[] explodeTrimSpace(String str, String separator) {
        String sep = "([\\s]+)?" + separator + "([\\s]+)?";
        return StringUtil.trimSpaces(str).replaceAll(sep, separator).split(separator);
    }

    public static List<String> explodeTrimSpaceToList(String str, String separator) {
        List<String> list = new ArrayList<String>();
        String[] replaced = StringUtil.explodeTrimSpace(str, separator);
        for(final String o : replaced) {
            list.add(o);
        }
        return list;
    }

    public static String[] explodeTrimSpace(String str) {
        return StringUtil.explodeTrimSpace(str, ",");
    }

    public static String implode(String implodes, String[] str)
    {
        return StringUtils.join(str, implodes);
    }

    public static String implode(String implodes, List<String> str)
    {
        return StringUtils.join(str.toArray(), implodes);
    }

    public static String implode(String implodes, String str)
    {
        return str;
    }

    public static String buildUnixPath(String[] paths) {
        return StringUtil.implode("/", paths);
    }

    public static String buildWindowsPath(String[] paths) {
        return StringUtil.implode("\\", paths);
    }

    public static String buildUnixPath(List<String> paths) {
        return StringUtil.implode("/", paths);
    }

    public static String buildWindowsPath(List<String> paths) {
        return StringUtil.implode("\\", paths);
    }

    public static String urlBuilder(String[] urls)
    {
        return StringUtil.buildUnixPath(urls);
    }

    public static String urlBuilder(List<String> urls)
    {
        return StringUtil.buildUnixPath(urls);
    }

}
