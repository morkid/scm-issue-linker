package com.morkid.scm.plugins.issuelinker.handler;

import com.morkid.scm.plugins.issuelinker.config.RepositoryConfig;
import com.morkid.scm.plugins.issuelinker.util.StringUtil;
import org.eclipse.jgit.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.BasicContextProvider;
import sonia.scm.issuetracker.LinkHandler;
import sonia.scm.issuetracker.TemplateBasedHandler;
import sonia.scm.repository.Repository;
import sonia.scm.template.TemplateEngineFactory;

import java.io.*;
import java.util.Scanner;

/**
 * IssueHandler
 *
 * @author Morkid <morkid@gmail.com>
 */
public abstract class IssueHandler extends TemplateBasedHandler implements Closeable {
    private static final Logger logger = LoggerFactory.getLogger(IssueHandler.class);

    private final RepositoryConfig config;

    private String username;

    protected IssueHandler(TemplateEngineFactory templateEngineFactory,
                           LinkHandler linkHandler, RepositoryConfig config) {
        super(templateEngineFactory, linkHandler);
        this.config = config;
    }

    protected RepositoryConfig getConfig() {
        return config;
    }

    @Override
    public void close() throws IOException {}

    protected void createRepositoryHook(Repository repository) {
        if (null != repository) {
            File base = getBaseDir();
            String[] paths = {
                "repositories",
                repository.getType(),
                repository.getName(),
                "hooks"
            };
            String childPath = StringUtil.implode(File.separator, paths);
            File basedir = new File(base, childPath);
            boolean exists = false;

            if (!basedir.exists()) {
                try {
                    exists = basedir.mkdirs();
                } catch (SecurityException e) {
                    // ..
                }
            } else {
                exists = true;
            }

            if (exists) {
                createHookFile(basedir.toString(), repository);
            }
        }
    }

    private File getBaseDir() {
        BasicContextProvider provider = new BasicContextProvider();
        return provider.getBaseDirectory();
    }

    private boolean createHookFile(String basedir, Repository repository) {
        if (null == basedir) {
            return false;
        }
        boolean success = false;
        boolean exists = false;

        String[] filePaths = {basedir, "post-receive"};
        String hookFilePath = StringUtil.implode(File.separator, filePaths);

        File script = new File(hookFilePath);
        String template = getHookTemplate();
        if (!script.exists() && !StringUtils.isEmptyOrNull(template)) {
            try {
                exists = script.createNewFile();
            } catch (IOException e) {
                // ..
            } catch (RuntimeException e) {
                // ..
            }
        }

        try {
            if (exists) {
                PrintWriter writer = new PrintWriter(script, "UTF-8");
                writer.println(template);
                writer.close();
                success = script.setExecutable(true);
                // Runtime.getRuntime().exec("chmod +x "+script.toString());
            }
        } catch (FileNotFoundException e) {
            // ..
        } catch (UnsupportedEncodingException e) {
            // ..
        } catch (RuntimeException e) {
            // ..
        }

        return success;
    }

    private String getHookTemplate() {
        String dir = getBaseDir().toString();
        String[] templatePath = {
            dir,
            "var",
            "scripts",
            "hooks",
            "post-receive"
        };
        String templatePaths = StringUtil.implode(File.separator, templatePath);
        String template = null;
        File path = new File(templatePaths);
        if (path.exists() && path.isFile() && path.canRead()) {
            try {
                template = (new Scanner(path)).useDelimiter("\\Z").next();
            } catch (IOException e) {
                // ..
            }
        }

        return template;
    }
}
