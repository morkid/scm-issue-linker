package com.morkid.scm.plugins.issuelinker.handler;

import com.google.common.base.Strings;
import com.morkid.scm.plugins.issuelinker.config.RepositoryConfig;
import com.morkid.scm.plugins.issuelinker.util.HttpRequestUtil;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.config.ScmConfiguration;
import sonia.scm.issuetracker.IssueRequest;
import sonia.scm.issuetracker.LinkHandler;
import sonia.scm.repository.Changeset;
import sonia.scm.repository.Person;
import sonia.scm.repository.Repository;
import sonia.scm.template.Template;
import sonia.scm.template.TemplateEngine;
import sonia.scm.template.TemplateEngineFactory;

import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.math.BigInteger;

/**
 * CommentHandler
 *
 * @author Morkid <morkid@gmail.com>
 */
public class CommentHandler extends IssueHandler implements sonia.scm.issuetracker.CommentHandler
{
    private static final String TEMPLATE = "com/morkid/scm/plugins/issuelinker/template/update.mustache";

    private final IssueRequest request;

    private ScmConfiguration configuration;

    private static final Logger logger = LoggerFactory.getLogger(CommentHandler.class);

    public CommentHandler(TemplateEngineFactory templateEngineFactory,
                          LinkHandler linkHandler, RepositoryConfig config,
                          IssueRequest request, ScmConfiguration configuration)
    {
        super(templateEngineFactory, linkHandler, config);
        this.request = request;
        this.configuration = configuration;
    }

    @Override
    public void commentIssue(String issueKey) {
        BigInteger key = new BigInteger(issueKey);

        String message = createComment(request);
        Repository repo = request.getRepository();
        RepositoryConfig config = getConfig();
        if (null != repo) {
            createRepositoryHook(repo);
        }
        if (config != null && config.isValid() && !Strings.isNullOrEmpty(message) && null != repo) {
            MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
            Changeset cs = request.getChangeset();
            Person user = cs.getAuthor();

            formData.add("issueId", key.toString());
            formData.add("issueMessage", message);
            formData.add("issueKeyword", "");
            formData.add("id", repo.getId());
            formData.add("path", repo.createUrl(configuration.getBaseUrl()));
            formData.add("name", repo.getName());
            formData.add("userEmail", user.getMail());
            formData.add("userName", user.getName());

            HttpRequestUtil http = new HttpRequestUtil(config.getFullPathUpdate());
            http.setUserPass(config.getUsername(), config.getPassword());
            http.post(formData);
        }
    }

    @Override
    protected Template loadTemplate(TemplateEngine engine) throws IOException {
        return engine.getTemplate(TEMPLATE);
    }
}
