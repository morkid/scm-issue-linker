package com.morkid.scm.plugins.issuelinker.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.repository.Repository;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(name = "issuelinker")
@XmlAccessorType(XmlAccessType.FIELD)
public class AppConfig extends RepositoryConfig
{
    @XmlElement(name = "disable-repository-configuration")
    private boolean repositoryConfigDisabled;

    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);

    public AppConfig()
    {
    }

    public AppConfig(Repository repository)
    {
        super(repository);
    }

    /**
     * Is Repository Config Disabled
     */
    @Override
    public boolean isRepositoryConfigDisabled()
    {
        return repositoryConfigDisabled;
    }
}
