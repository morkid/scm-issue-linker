package com.morkid.scm.plugins.issuelinker.resource;

import com.google.inject.Inject;
import com.morkid.scm.plugins.issuelinker.IssueLinker;
import com.morkid.scm.plugins.issuelinker.config.AppConfig;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.security.Role;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * ConfigResource
 *
 * @author Morkid <morkid@gmail.com>
 */
@Path("plugins/morkid/issuelinker/issues/app-config")
public class ConfigResource {
    private static final Logger logger = LoggerFactory.getLogger(ConfigResource.class);

    private IssueLinker linker;

    @Inject
    public ConfigResource(IssueLinker linker) {
        if (!SecurityUtils.getSubject().hasRole(Role.ADMIN)) {
            logger.warn("user has not enough privileges to configure issuelinker");

            throw new WebApplicationException(Response.Status.FORBIDDEN);
        }

        this.linker = linker;
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response updateConfiguration(AppConfig updatedConfig) {
        linker.setDefaultConfig(updatedConfig);

        return Response.ok().build();
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getConfiguration() {
        return Response.ok(linker.getDefaultConfig()).build();
    }
}
