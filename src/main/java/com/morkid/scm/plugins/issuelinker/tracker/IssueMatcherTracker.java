package com.morkid.scm.plugins.issuelinker.tracker;

import com.google.common.base.Strings;
import com.morkid.scm.plugins.issuelinker.config.RepositoryConfig;
import sonia.scm.issuetracker.IssueMatcher;
import sonia.scm.util.HttpUtil;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IssueMatcherTracker implements IssueMatcher {
    private static final String REPLACEMENT_LINK =
        "<a target=\"_blank\" href=\"{0}{1}\">$0</a>";

    private static final Pattern KEY_PATTERN = Pattern.compile("(\\#)([0-9]{1,10})");

    public IssueMatcherTracker(RepositoryConfig configuration) {
        this.configuration = configuration;
    }

    @Override
    public String getKey(Matcher matcher) {
        String match = matcher.group(2);
        if (Strings.isNullOrEmpty(match)) {
            return null;
        }
        return matcher.group(2).replaceAll("^[0]+", "");
    }

    @Override
    public Pattern getKeyPattern() {
        return KEY_PATTERN;
    }

    @Override
    public String getReplacement(Matcher matcher) {
        String replacement = null;
        String url = configuration.getFullPathLink();

        if (!Strings.isNullOrEmpty(url)) {
            url = HttpUtil.getUriWithoutEndSeperator(url);

            replacement = MessageFormat.format(REPLACEMENT_LINK, url, getKey(matcher));
        }

        return replacement;
    }

    private final RepositoryConfig configuration;
}
