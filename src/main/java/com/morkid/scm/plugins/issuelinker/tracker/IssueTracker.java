package com.morkid.scm.plugins.issuelinker.tracker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * IssueTracker
 *
 * @author Morkid <morkid@gmail.com>
 */
public class IssueTracker
{
    private static final Logger logger = LoggerFactory.getLogger(IssueTracker.class);

    private String id;

    private String title = "Unknown";

    private String status = "Unknown";

    private String message = "";

    private String level = "Unknown";

    private String url = "#";

    private ListIterator<CommentTracker> comments;

    public IssueTracker() {}

    public IssueTracker(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ListIterator<CommentTracker> getComments() {
        return comments;
    }

    public void addComment(CommentTracker comment) {
        this.comments.add(comment);
    }
}
