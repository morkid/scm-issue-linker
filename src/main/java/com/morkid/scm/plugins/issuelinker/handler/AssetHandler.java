package com.morkid.scm.plugins.issuelinker.handler;

import com.google.inject.Inject;
import com.morkid.scm.plugins.issuelinker.IssueLinker;
import sonia.scm.plugin.ext.Extension;
import sonia.scm.resources.ResourceHandler;
import sonia.scm.resources.ResourceType;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Extension
public class AssetHandler implements ResourceHandler {
    private static final String PATH = "/com/morkid/scm/plugins/issuelinker/resources/RepositoryConfig.js";

    private final IssueLinker linker;

    @Inject
    public AssetHandler(IssueLinker linker) {
        this.linker = linker;
    }

    @Override
    public String getName() {
        return PATH;
    }

    @Override
    public InputStream getResource() {
        InputStream content;

        if (!linker.getDefaultConfig().isRepositoryConfigDisabled()) {
            content = AssetHandler.class.getResourceAsStream(PATH);
        } else {
            content = new ByteArrayInputStream(new byte[0]);
        }

        return content;
    }

    @Override
    public ResourceType getType() {
        return ResourceType.SCRIPT;
    }
}
