package com.morkid.scm.plugins.issuelinker.handler;

import com.google.common.base.Strings;
import com.morkid.scm.plugins.issuelinker.config.RepositoryConfig;
import com.morkid.scm.plugins.issuelinker.tracker.RepositoryTracker;
import com.morkid.scm.plugins.issuelinker.util.HttpRequestUtil;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sonia.scm.SCMContextProvider;
import sonia.scm.config.ScmConfiguration;
import sonia.scm.issuetracker.ChangeStateHandler;
import sonia.scm.issuetracker.IssueRequest;
import sonia.scm.issuetracker.LinkHandler;
import sonia.scm.repository.Changeset;
import sonia.scm.repository.Person;
import sonia.scm.repository.Repository;
import sonia.scm.template.Template;
import sonia.scm.template.TemplateEngine;
import sonia.scm.template.TemplateEngineFactory;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.math.BigInteger;
import java.rmi.RemoteException;

/**
 * ChangesetHandler
 *
 * @author Morkid <morkid@gmail.com>
 */
public class ChangesetHandler extends IssueHandler implements ChangeStateHandler
{
    private static final Logger logger = LoggerFactory.getLogger(ChangesetHandler.class);

    private static final String TEMPLATE = "com/morkid/scm/plugins/issuelinker/template/changeState.mustache";

    private final IssueRequest request;

    private RepositoryTracker tracker;

    private ScmConfiguration configuration;

    public ChangesetHandler(TemplateEngineFactory templateEngineFactory,
                            LinkHandler linkHandler, RepositoryConfig config,
                            IssueRequest request, ScmConfiguration configuration)
    {
        super(templateEngineFactory, linkHandler, config);
        this.request = request;
        this.configuration = configuration;
    }

    @Override
    public void changeState(String issueKey, String keyword) {
        BigInteger key = new BigInteger(issueKey);

        try {
            changeState(key, keyword);
        } catch (RemoteException ex) {
            logger.error("could not change state", ex);
        }
    }

    private void changeState(BigInteger key, String keyword) throws RemoteException
    {
        String message = createComment(request, keyword);
        Repository repo = request.getRepository();
        RepositoryConfig config = getConfig();

        if (null != repo) {
            createRepositoryHook(repo);
        }

        if (config != null && config.isValid() && !Strings.isNullOrEmpty(message) && null != repo) {
            System.out.println(message);
            MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
            Changeset cs = request.getChangeset();
            Person user = cs.getAuthor();

            formData.add("issueId", key.toString());
            formData.add("issueMessage", message);
            formData.add("issueKeyword", keyword);
            formData.add("id", repo.getId());
            formData.add("path", repo.createUrl(configuration.getBaseUrl()));
            formData.add("name", repo.getName());
            formData.add("userEmail", user.getMail());
            formData.add("userName", user.getName());

            HttpRequestUtil http = new HttpRequestUtil(config.getFullPathUpdate());
            http.setUserPass(config.getUsername(), config.getPassword());
            http.post(formData);
        }
    }

    @Override
    public Iterable<String> getKeywords() {
        return getConfig().getKeywords();
    }

    @Override
    protected Template loadTemplate(TemplateEngine engine) throws IOException {
        return engine.getTemplate(TEMPLATE);
    }

}
